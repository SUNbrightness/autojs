  //大润发生鲜刷运力

"ui";
ui.layout(
    <vertical>
      <text id="name" text="大润发GO" textSize="22sp" textColor="#fbfbfe" bg="#00afff" w="*" gravity="center"></text>
      <button id="start">开始</button>
      <button id="stop">停止</button>
 <text  textColor="green">疫情之下，注意调整身心别给自己制造无形的压力，焦虑来源是你自己</text>
 <text  textColor="red">用这个软件一定行，别放弃,加油！</text>
    </vertical>
  );
  


    //等待开启无障碍模式
    auto();

  ui.start.click(function () {
      

    threads.start(function () {
        startRing();
        main();
    });
  });

  ui.stop.click(function () {
   exit();
  });


function main(){
    

    while(true){
        payAction();
    }
}


function payAction() {

    goToCar();


    const goPayId = id('com.rt.market.fresh:id/btn_submit');
   
    //如果5秒进入按钮还不出现，就重新开始
    if(!noteExistFor(goPayId,5000)){
        payAction();
        return;
    }


    //按钮出现
    var goPay = goPayId.findOnce();
    //可以点击
    if(goPay&&goPay.enabled()){
        goPay.click();

        //尝试付款
        clickAtOncePay(1);

        startRing();
    }else{
        payAction();
        
    }

}

function clickAtOncePay(num) {
    num = num | 5;
    
    const submitId = id('com.rt.market.fresh:id/tv_submit');
    //这个按钮容易刷新不出来,尝试等待
    noteExistFor(submitId,2000);

    var atoncPay = submitId.findOnce();
    verifyNode(atoncPay, '去结算失败!');

    //多点几次
    for(var i=0;i<num;i++){
        log('atoncePay'+i);
        atoncPay.click();
        //按钮找不到了就退出
        if(!noteExistFor(submitId,0)){
            log('结算按钮找不到了')
            break;
        }
        sleep(100);
    }
    
}

function goToCar(){
    //这样启动没有广告，有点android开发经验，贼爽
    app.startActivity({
        action: "android.intent.action.VIEW",
        className: "com.rt.market.fresh.welcome.activity.WelcomeActivity",
        packageName: "com.rt.market.fresh",
    });
    
    
    const carId =text('购物车');

    //尝试等待购物车按钮出现
    noteExistFor(carId,5000);

    
    var gotoCar = carId.findOnce();
    verifyNode(gotoCar, '启动叮咚买菜失败!');
    toXyClick(gotoCar);


    //尝试全选商品
    // var cbAllId = id('com.yaya.zone:id/cb_all');
    // //等会一会看看全选按钮是否出现
    // noteExistFor(carId,1000);

    // if(cbAllId.exists()  &&!cbAllId.findOne().checked()){
    //     toXyClick(cbAllId.findOne())
    //     sleep(2000);
    // }

}


//直接蹦出弹框重新加载
function tryReloadDialog(){
    
    var reloadDialog = id('com.yaya.zone:id/img_status').findOnce();
  
    while(reloadDialog){
        id('com.yaya.zone:id/ll_reload_action').findOnce().click();
        sleep(200);
        reloadDialog = id('com.yaya.zone:id/img_status').findOnce();
    }

}


//提示部分商品不足知否继续
function tryDeficientDialog(){
    var gotoPayButton = id('com.yaya.zone:id/tv_goto_pay').findOnce();
    if(gotoPayButton){
        gotoPayButton.click();
        sleep(500);
    }
}


//如果提示当前时间运力不足就自动选择一个空余运力
function trySelectHour() {

    //尝试寻找一个可用时间段
    var selectHours = id('com.yaya.zone:id/cl_item_select_hour_root').find();

    if (selectHours.empty()) {
        //为空说明未弹出提示框
        return false;
    }
    //便利所有时间
    for(var hour of selectHours){
        //有一个ok就选中它
        if(hour.enabled()){
            //选中
            hour.click();
            sleep(500);
            //返回选中成功
            return true;
        }
    }
    return false;
}


//尝试成功
function trySucess() {

    var isSuccess=false;



    //购物车提示为空，有可能刷不出购物车，因此不能用购物车为空来判断下单完成
    // var ttd = '购物车还是空的';    
    // if(id("com.yaya.zone:id/tv_header_empty_str").text(ttd).exists()){
    //     isSuccess=true;
    //  }


    if(id("com.yaya.zone:id/tv_sum_money").textContains('0.00').exists()){
        //金钱为空不存在
        isSuccess=true;
    }

    //购物车未清空
    if(!isSuccess){
        return;
    }

    //胜利号角
    for(var i=0;i<20;i++){
        startRing();
        shake();
        toastLog("购物车为空，检查是否购物成功!!");
        sleep(1000);
    }
    exit();
}

function startRing(duration, volume) {
    media.playMusic('/sdcard/Music/卜卦.mp3', 0.8);
} 


function shake(vibrate_time) { var vibrate_time = vibrate_time || 1000; device.vibrate(vibrate_time); }


//尝试等待节点出现
function noteExistFor(se,ms){
    ms = ms || 1000;
    while(ms>0){
        if(se.exists()){
            return true;
        }
        sleep(100);
        ms = ms -100;
    }
    //不存在
    return false;
}


function verifyNode(node, msg) {
    if (!msg) {
        msg = "流程错误";
    }
    if (!node) {
        toastLog(msg);
        toastLog('已经重新执行程序');
        main();
    }
}

//部分APP无法直接触发控件事件
function toXyClick(item){
    click(item.bounds().centerX(),item.bounds().centerY())
  }